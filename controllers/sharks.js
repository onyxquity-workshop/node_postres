const path = require('path');

exports.index = function (req, res) {
    res.sendFile(path.resolve('views/sharks.html'));
};

const { pool } = require('../db')

exports.create = function(req, res) {
    const sql = "INSERT INTO Shark (name, character) VALUES ($1, $2)";
    const shark = [req.body.name, req.body.character];
     
    pool.query(sql, shark, (error, result) => {
         if(error) {
             res.status(400).send('Unable to save shark to database') 
         } else {
             res.redirect('/sharks/getshark');
        }
     }
    );
};


exports.list = function(req, res) {
  pool.query("SELECT * FROM Shark", (error, sharks) => {
    if (error) {
            return res.send(500, error); 
        }
        res.render('getshark', {
             sharks: sharks.rows
        });
  })
}




